package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

func NewLifts() []Lift {
	lifts := []Lift{
		{0, 0, 0, []MoveRequest{}},
		{1, 0, 0, []MoveRequest{}},
		{2, 0, 0, []MoveRequest{}},
	}
	return lifts
}

type Server struct {
	router *mux.Router
	lifts  []Lift
}

func NewServer() *Server {
	lifts := NewLifts()
	router := mux.NewRouter()

	api := router.PathPrefix("/api/").Subrouter()
	api.HandleFunc("/lifts", GetLiftsStatus).Methods("GET")
	api.HandleFunc("/lift", GetLiftStatus).Methods("GET")
	api.HandleFunc("/move/{id:[0-2]}", MoveLift).Methods("PUT")

	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./build/static/"))))
	router.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./build/index.html")
	})

	s := &Server{router, lifts}
	return s
}

func GetLiftsStatus(w http.ResponseWriter, r *http.Request) {

}

func GetLiftStatus(w http.ResponseWriter, r *http.Request) {

}

func MoveLift(w http.ResponseWriter, r *http.Request) {

}
