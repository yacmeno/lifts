package main

import (
	"errors"
)

type Lift struct {
	id           int
	currentFloor int
	state        State
	mistakes     []MoveRequest
}

type State int

const (
	Idle State = iota
	MovingUp
	MovingDown
)

func (l *Lift) SetState(s State) {
	l.state = s
}

type MoveRequest struct {
	srcFloor int
	dstFloor int
	dir      Direction
}

type Direction int

const (
	Up Direction = iota
	Down
)

func (l *Lift) Move(r MoveRequest) error {
	switch l.state {
	case Idle:
		if r.dir == Up {
			l.SetState(MovingUp)
		} else {
			l.SetState(MovingDown)
		}
		l.currentFloor = r.srcFloor // kinda useless since instant?
		l.Move(r)
		return nil
	case MovingUp:
		if r.dir == Up {
			l.currentFloor = r.dstFloor
		}
		return nil
	case MovingDown:
		if r.dir == Down {
			l.currentFloor = r.dstFloor
		}
		return nil
	default:
		return errors.New("invalid movement")
	}
}

// MoveMany represents the situation where one or many people enter the lift in the same direction
// Since instant travel is expected, there is no optimization necessary.
// Otherwise would need to consider the lift never going down even when state is MovingUp, PriorityQueue implementation, dead time for people waiting for a lift
// This function handles people entering by mistake
func MoveMany(l *Lift, reqs []MoveRequest) {
	for _, m := range reqs {
		if m.dir == Up && l.state == MovingDown {
			l.mistakes = append(l.mistakes, m)
		} else if m.dir == Down && l.state == MovingUp {
			l.mistakes = append(l.mistakes, m)
		}
		l.Move(m)
	}

	if l.state == MovingUp {
		l.SetState(MovingDown)
	} else if l.state == MovingDown {
		l.SetState(MovingUp)
	}

	for _, m := range l.mistakes {
		l.Move(m)
	}

	l.mistakes = []MoveRequest{}
	l.SetState(Idle)
}
