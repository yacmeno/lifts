"use strict";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lifts: [],
    };
  }

  componentDidMount() {
    fetch("/api/lifts").then((_) =>
      this.setState({
        lifts: [{ id: 0 }, { id: 1 }, { id: 2 }],
      })
    );
  }

  render() { 
    return this.state.lifts.map((lift) => <Lift id={lift.id} />)
  }
}

class Lift extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <div>{this.props.id}</div>;
  }
}

class InputBox extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <input type="text"></input>
    )
  }
}

const domContainer = document.querySelector("#app");
ReactDOM.render(<App />, domContainer);
