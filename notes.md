# Steps
- Start with 1 lift that can go up and down
	- Lift has 3 states: MovingUp, MovingDown, Idle
	- Lift should only pickup passengers that go up when its state is MovingUp, that go down when state is MovingDown
	- Idle state occurs when: there are no more passengers
	- Lift's currentFloor attribute changes when: dropping someone, picking up someone, idle
	- Scheduling: happens when many people are in the lift and queue buttons not in order (try to use priority queue instead of sorting). But since instant movement, no need?
- What changes with many lifts?
	- Since it's instant, nothing?
	- If it wasn't instant: 
		- No need to wait for a lift that goes in the same direction


# UI scenario
- 3 lifts on screen
- you can input an array of objects of the form: {floor: 12, nbPassengers: 3, [3]MoveRequest}, then assign the appropriate lift and execute the moves
- show history on each lift

## Current problems that are acceptable
- There's no concept of queueing
- lifts can go down even when their state is MovingUp