package main

import (
	"log"
	"net/http"
)

func main() {
	s := NewServer()
	log.Fatal(http.ListenAndServe(":8080", s.router))
}
