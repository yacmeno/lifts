package main

import (
	"testing"
)

func TestMove(t *testing.T) {
	t.Run("Should move to requested floor from idle state", func(t *testing.T) {
		l := Lift{0, 0, Idle, []MoveRequest{}}
		req := MoveRequest{0, 3, Up}
		l.Move(req)

		got := struct {
			state State
			floor int
		}{l.state, l.currentFloor}
		want := struct {
			state State
			floor int
		}{MovingUp, 3}

		if got != want {
			t.Errorf("lift should toggle from idle state and reach expected floor")
		}
	})

	t.Run("Should handle many passengers entering in the same direction", func(t *testing.T) {
		l := Lift{0, 0, Idle, []MoveRequest{}}
		// 3 passengers enter at floor 12 and go up
		// 3 other passengers enter at floor 17 and go up
		reqs := []MoveRequest{
			{12, 14, Up},
			{12, 24, Up},
			{12, 18, Up},
			{17, 18, Up},
			{17, 29, Up},
			{17, 20, Up},
		}

		MoveMany(&l, reqs)
		got := struct {
			finalFloor int
			finalState State
		}{l.currentFloor, l.state}

		lastFloorRequested := reqs[len(reqs)-1].dstFloor
		want := struct {
			finalFloor int
			finalState State
		}{lastFloorRequested, Idle}

		if got != want {
			t.Errorf("lift should be at last floor requested and back to idle state")
		}
	})

	t.Run("Should handle people going opposite direction (mistaken passengers)", func(t *testing.T) {
		l := Lift{0, 0, Idle, []MoveRequest{}}
		reqs := []MoveRequest{
			{12, 14, Up},
			{12, 8, Down},
			{12, 18, Up},
			{14, 3, Down},
			{11, 21, Up},
		}

		MoveMany(&l, reqs)
		got := struct {
			finalFloor int
			finalState State
		}{l.currentFloor, l.state}

		lastMistake := reqs[len(reqs)-2]
		want := struct {
			finalFloor int
			finalState State
		}{lastMistake.dstFloor, Idle}

		if got != want {
			t.Errorf("lift should handle passengers entering in the wrong direction")
		}
	})
}
