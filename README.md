# Simple Lift
Create the API and the frontend which implement the behavior of a lift.

Behavior of a lift:
* a lift responds to calls containing a source floor and direction
* a lift has an attribute floor, which describes it’s current location
* a lift delivers passengers to requested floors


## API
* api may manage more than one lift
* api contains an endpoint to get a lift state
* api contains an endpoint to which set a lift
* Lifts state can be in-memory for the sake of the exercice
* You can use the `nodejs` or `golang` or `java`. Take the one you are the most used to.
* We do not expect a full lift scheduling algorithm to be implemented. Lifts move instantaneously

## Webapp
* webapp display the state of a lift
* webapp can change the state of a lift
* webapp must be themed with white and robotiq blue `#00acff`
* reactjs framework must be used

TDD, KISS approaches and implementation of SOLID principles are encouraged.

## Q&A
Hardcode number of lifts.

Hardcode number of floors. But lifts can have different number of floors assigned to them.

Scheduling due to users behavior is present. But not due to lift behavior (instantaneous movement).



